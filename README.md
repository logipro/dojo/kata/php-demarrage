# php-demarrage

Pour disposer d'un environnement standard sur un ordinateur équipé de docker:
* php (version recente)
* composer, phpunit
* * dossier tests pour écrire les tests unitaires avec phpunit
* * dossier src pour écrire le code source
* codecheck pour imposer le typage en PHP, la vérification de la syntaxe et le passage des tests

# Installation

```bash
git clone git@gitlab.com:logipro/dojo/kata/php-demarrage.git
cd php-demarrage
./install
```

# Lancement

Lancement des serveurs "dockerisé" locaux:
```bash
bin/start

# pour arreter:
bin/stop
```

Avec un navigateur: http://localhost:30080

Les mails émis par SMTP peuvent être géré par le mailcatcher . Les mails aboutissent définitivement dans le mailcatcher. Une interface permettant d'examiner les mails:

http://localhost:49080



# Commit
Avant chaque "commit" : vérifications du typage, de la syntaxe, du passage des tests unitaires.

```bash
./codecheck
```


# Outils de tests

## PHPUnit

```console
bin/phpunit
```

## Infection

```console
bin/infection
```

# Autre chose ?

Appel direct au langage, par exemple la version utilisée
```bash
bin/php --version
```

Appel à divers outils de vérification de la qualité
```bash
bin/qa
```

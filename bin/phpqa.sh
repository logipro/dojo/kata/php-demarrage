#!/bin/bash

qa()
{
    docker run \
        --init \
        --rm \
        -ti \
        --user 1000:1000 \
        -v "$(pwd):/usr/src" \
        -v "$(pwd)/tmp:/tmp" \
        -v "$(pwd)/bin/docker/php/php.ini:/usr/local/etc/php/php.ini" \
        -w /usr/src jakzal/phpqa:php8.1-alpine "$@"
}
